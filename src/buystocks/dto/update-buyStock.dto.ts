import { PartialType } from '@nestjs/swagger';
import { CreateBuyStockDto } from './create-buyStock.dto';

export class UpdateBuyStockDto extends PartialType(CreateBuyStockDto) {}
