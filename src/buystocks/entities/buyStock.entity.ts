import { BuystockorderItem } from 'src/buystock-orders/entities/buystock-orderitem.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class BuyStock {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  unit: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(
    () => BuystockorderItem,
    (buystockorderItems) => buystockorderItems.buystock,
  )
  buystockorderItems: BuystockorderItem[];
}
