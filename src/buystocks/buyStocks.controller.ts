import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { BuyStocksService } from './buyStocks.service';
import { CreateBuyStockDto } from './dto/create-buyStock.dto';
import { UpdateBuyStockDto } from './dto/update-buyStock.dto';
import { AuthGuard } from 'src/auth/auth.guard';

@UseGuards(AuthGuard)
@Controller('buyStocks')
export class BuyStocksController {
  constructor(private readonly buyStocksService: BuyStocksService) {}
  @Post()
  create(@Body() createBuyStockDto: CreateBuyStockDto) {
    return this.buyStocksService.create(createBuyStockDto);
  }

  @Get()
  findAll() {
    return this.buyStocksService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.buyStocksService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateBuyStockDto: UpdateBuyStockDto,
  ) {
    return this.buyStocksService.update(+id, updateBuyStockDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.buyStocksService.remove(+id);
  }
}
